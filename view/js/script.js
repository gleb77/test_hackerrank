const headers = ['SL', 'Date', 'Diagnosis', 'Weight', 'Doctor'];
function fetchDataById (ID) {
  document.getElementById("loader-view").style = "display: block";
    fetch(`https://jsonmock.hackerrank.com/api/medical_records?userId=${ID}`)
    .then((response) => {
    document.getElementById("loader-view").style = "display: none";
    return response.json();
  })
  .then((response) => {
    const {userName, userDob, meta: {height}} = response.data[0];
    setProfileView({userName, userDob, height})
    createTabelHead()
    createTabelBody(response.data)
    return response.data;
  });
}

function getActiveValue () {
  const select = document.getElementById("patient-select");
  return select.options[select.selectedIndex].value;
}

function change() {
  const value = getActiveValue();
  setLoaderStyle(value);
}

document.getElementById("submit-btn").addEventListener('click', () => onButtonClick())

function setProfileView ({userName, userDob, height}) {
  document.getElementById("patient-name").innerHTML = userName;
  document.getElementById("patient-dob").innerHTML = userDob.split("-").join("/");
  document.getElementById("patient-height").innerHTML = `Heigh: {${height}}`;
}

  function setLoaderStyle (value) {
    if(value !== -1) {
      document.getElementById("loader-view").style = "display: block";
    }
    document.getElementById("loader-view").style = "display: none";
  }

  function onButtonClick () {
    const id = getActiveValue();
    id != -1 && fetchDataById(id);
    return false;
  }

  function createTabelHead () {
    const thead = document.getElementById('table-header')
    if ( thead.hasChildNodes() ) { 
      thead.removeChild( thead.childNodes );
    }
    const th = document.createElement("tr");
    for(let i = 0; i < headers.length; i++) {
      let td = document.createElement("td");
      td.innerHTML = headers[i]
      th.appendChild(td)
    }
    thead.appendChild(th);
  }

  function createTabelBody (list) {
    let tbody = document.getElementById('table-body');
    if ( tbody.hasChildNodes() ) { 
      tbody.removeChild( tbody.childNodes );
    }
    for(let i = 0; i < list.length; i++) {
      let tr = tbody.insertRow(i);
      tr.insertCell(0).innerHTML = list[i].userName;
      tr.insertCell(1).innerHTML = parseData(list[i].timestamp);
      tr.insertCell(2).innerHTML = list[i].diagnosis.name;
      tr.insertCell(3).innerHTML = list[i].meta.weight;
      tr.insertCell(4).innerHTML = list[i].doctor.name;
    }
  }

  function parseData(timestamp) {
    const date = new Date(timestamp)
    const day = date.getDate();
    const month = date.getMonth();
    const year = date.getFullYear();
    return day + "/" + month + "/" + year;
}
